/***********************************************************************
* FILE: ser_prime.c
* DESCRIPTION: 
*   This program generates primes. The approach taken is a "brute force"
*   method which requires increasingly greater amounts of cpu as the problem
*   size increases.  It should lend itself well to an embarassingly parallel
*   solution since each prime can be computed independently of all other
*   primes.
* AUTHOR: Blaise Barney 11/25/95 - adapted from version contributed by 
*   Richard Ng & Wong Sze Cheong during MHPCC Singapore Workshop (8/22/95).
* LAST REVISED: 04/12/05
***********************************************************************/

/*
The purpose is to obtain the number of primes found in the range:        
[0 --> LIMIT]
and to keep track of the largest prime yet. 
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define LIMIT     102000040    /* Increase this to find more primes */
#define PRINT     1000000     /* Print a line after this many numbers */


// A function that check if a number (n) is a prime or not
int isprime(int n) {

    // Declare integer variables i and squareroot
    int i, squareroot;

    // If n is greater than 10
    if (n > 10) {
        // Find the square root of the the number
        squareroot = (int) sqrt(n);

        // Starting from 3 until (and including) the square root calculated with strides of 2 (to avoid even numbers)
        for (i = 3; i<=squareroot; i = i + 2)
            // If i is a perfect divisor of n
            if ((n % i)==0)
                // return 0 (False)
                return 0;
        // return 1 (True)
        return 1;
    }
    /* Assume first four primes are counted elsewhere. Forget everything else */
    else

    return 0;
}


int main(int argc, char *argv[]) {

    // Create a file pointer
    // FILE *fptr = fopen("output/cores_1.csv", "w"); 
    // if (fptr == NULL) 
    // { 
    //     printf("Could not open file"); 
    //     return 0; 
    // } 


    // Initialize Timer
    clock_t t0 = clock();

    // Declare variables (n, pc and foundone)
    int n,                           /* loop variables */
        pc,                          /* prime counter */
        foundone;                    /* most recent prime found */

    // printf("Starting. Numbers to be scanned=%d\n", LIMIT);
    // fprintf(fptr, "numbers_scanned,primes_found\n");
    printf("numbers_scanned,primes_found\n");
    /* Assume the primes less than 10 (2,3,5,7) are counted here */
    pc=4;     

    // Loop over the range while taking a stride of 2 (to check for odd numbers only)
    for (n=11; n<=LIMIT; n=n+2) {

        // Pass the current number (n) to the isprime() function, and if True:
        if (isprime(n)) {
            // Increment the prime count
            pc++;
            // Track the largest prime yet
            foundone = n;
            }			
        // Print the progress every PRINT (interval set at the top)
        if ( (n - 1) % PRINT == 0 ) {
            // printf("Numbers scanned=%d --- Primes found=%d\n", n-1, pc);
            printf("%d,%d\n", n-1, pc);
            // fprintf(fptr, "%d,%d\n", n-1, pc);
            }

        }

    // Close the file
    // fclose(fptr); 

    // Print the largest prime and the total number of primes
    printf("Done. Largest prime is %d Total primes %d\n", foundone, pc);

    // Stop Timer
    clock_t t1 = clock();

    // Calculate time elapsed
    double time_elapsed = (double)(t1 - t0) / CLOCKS_PER_SEC;

    // Print time elapsed
    printf("%.2lf\n", time_elapsed);
} 
