/***********************************************************************
* FILE: mpi_prime.c
* DESCRIPTION:
*   Generates prime numbers.  All tasks distribute the work evenly, taking
*   every nth number, where n is the stride computed as:  (rank *2) + 1
*   so that even numbers are automatically skipped.  The method of using
*   stride is preferred over contiguous blocks of numbers, since numbers
*   in the higher range require more work to compute and may result in 
*   load imbalance.  This program demonstrates embarrassing parallelism.
*   Collective communications calls are used to reduce the only two data
*   elements requiring communications: the number of primes found and
*   the largest prime.
* AUTHOR: Blaise Barney 11/25/95 - adapted from version contributed by 
*   Richard Ng &  Wong Sze Cheong during MHPCC Singapore Workshop (8/22/95).
* LAST REVISED: 04/13/05
***********************************************************************/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define LIMIT     102400000  /* Increase this to find more primes */
#define PRINT     1000000     /* Print a line after this many numbers */
#define FIRST     0           /* Rank of first task */

// A function that check if a number (n) is a prime or not
int isprime(int n) {

    // Declare integer variables i and squareroot
    int i, squareroot;

    // If n is greater than 10
    if (n > 10) {
        // Find the square root of the the number
        squareroot = (int) sqrt(n);

        // Starting from 3 until (and including) the square root calculated with strides of 2 (to avoid even numbers)
        for (i = 3; i<=squareroot; i = i + 2)
            // If i is a perfect divisor of n
            if ((n % i)==0)
                // return 0 (False)
                return 0;
        // return 1 (True)
        return 1;
    }
    /* Assume first four primes are counted elsewhere. Forget everything else */
    else
    return 0;
}


int main (int argc, char *argv[]) {

    

    int ntasks,               /* total number of tasks in partitiion */
        rank,                 /* task identifier */
        n,                    /* loop variable */
        pc,                   /* prime counter */
        pcsum,                /* number of primes found by all tasks */
        foundone,             /* most recent prime found */
        maxprime,             /* largest prime found */
        mystart,              /* where to start calculating */
        stride;               /* calculate every nth number */


    double start_time, end_time;
        
    MPI_Init(&argc, &argv);                 // Initialize MPI computation
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);   // Get the rank of process
    MPI_Comm_size(MPI_COMM_WORLD, &ntasks); // Determine number of processes


    if (((ntasks%2) !=0) || ((LIMIT%ntasks) !=0)) {
        printf("Sorry - this exercise requires an even number of tasks.\n");
        printf("evenly divisible into %d.  Try 4 or 8.\n",LIMIT);
        MPI_Finalize(); // End MPI computation
    }

    start_time = MPI_Wtime();   /* Initialize start time */
    mystart = (rank*2)+1;       /* Find my starting point - must be odd number */
    stride = ntasks*2;            /* Determine stride*/
    pc=0;                       /* Initialize prime counter */
    foundone = 0;               /* Initialize */

    /**** RANK 0 - MASTER ****/
    if (rank == FIRST) {
        printf("Using %d tasks to scan %d numbers\n",ntasks,LIMIT);
        printf("numbers_scanned,primes_found\n");
        // printf("scanned,found\n");
        pc = 4;                  /* Assume first four primes are counted here */
        for (n=mystart; n<=LIMIT; n=n+stride) {
            if (isprime(n)) {
                pc++;
                foundone = n;
                // printf("%d, %d\n",rank, foundone);
            }
        
        
        // Print the progress every PRINT (interval set at the top)
        if ( (n - 1) % PRINT == 0 && (n-1) != 0) {

            // This can be the fromat of the output file:
            printf("%d %d\n", n - 1, pc);
            // fprintf(fptr, "%d,%d\n", n-1, pc);
            }

        }
        MPI_Reduce(
            &pc,            // Address of what we are sending (prime count)
            &pcsum,         // Address of what we are receiving (prime sum)
            1,              // Number of elements in the buffer
            MPI_INT,        // Datatype of what we are sending 
            MPI_SUM,        // Reduce operation
            FIRST,          // Rank of Root process (defined as 0)
            MPI_COMM_WORLD  // Default Communicator
        );
        MPI_Reduce(
            &foundone,      // Address of what we are sending (most recent prime)
            &maxprime,      // Address of what we are receiving (max prime)
            1,              // Number of elements in the buffer
            MPI_INT,        // Datatype of what we are sending 
            MPI_MAX,        // Reduce operation
            FIRST,          // Rank of Root process (defined as 0)
            MPI_COMM_WORLD  // Default Communicator
        );
        end_time=MPI_Wtime();

        printf("Done. Largest prime is %d Total primes %d\n",maxprime,pcsum);
        printf("%.2lf\n",end_time-start_time);
    }


    /**** RANKS (1-3) - SLAVES ****/
    if (rank > FIRST) {
        for (n=mystart; n<=LIMIT; n=n+stride) {
            if (isprime(n)) {
                pc++;
                foundone = n;
                // printf("%d, %d\n",rank, foundone);

            }
        }
        MPI_Reduce(
            &pc,            // Address of what we are sending (prime count)
            &pcsum,         // Address of what we are receiving (prime sum)
            1,              // Number of elements in the buffer
            MPI_INT,        // Datatype of what we are sending 
            MPI_SUM,        // Reduce operation
            FIRST,          // Rank of Root process (defined as 0)
            MPI_COMM_WORLD  // Default Communicator
        );
        MPI_Reduce(
            &foundone,      // Address of what we are sending (most recent prime)
            &maxprime,      // Address of what we are receiving (max prime)
            1,              // Number of elements in the buffer
            MPI_INT,        // Datatype of what we are sending 
            MPI_MAX,        // Reduce operation
            FIRST,          // Rank of Root process (defined as 0)
            MPI_COMM_WORLD  // Default Communicator
        );
    }

    MPI_Finalize();
}
