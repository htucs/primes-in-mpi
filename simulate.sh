#! /bin/bash

# We want it to run the code on:
# 1,    2,    4,    8,    16,    32,    64,    128 cores.
# 2^0,  2^1,  2^2,  2^3,  2^4,   2^5,   2^6,   2^7 cores.


# For the mpi_prime.c, the -np argument must take an exponent of 2 wrt to how the parallelization was made.
# However it will fail if we choose 2^0, as this equates 1 and is not divisible by 2, and we must run the serial version ser_prime.c.


######################################################################
# Approach 1
# Output the range

OUTPUT_FILE="output.dat"
echo "102400000" >> $OUTPUT_FILE &&

# Loop over the exponents {0-6}
for i in {0..7};
do
    # Create a variable (cores) that equates to 2^exponent
    let cores=$((2**$i)) &&
    # If the exponent is 0
    if [ $i -eq 0 ]; then
        # Run the Serial version
        t=$(./ser_prime | tail -n 1)  &&
        echo $cores,$t >> $OUTPUT_FILE &&
        echo -e "Completed run with $cores cores in $t seconds" 
    # If the exponent is not 0
    else
        # Run the MPI version with the cores variables
        t=$(mpiexec -np $cores ./mpi_prime | tail -n 1) &&
        echo $cores,$t >> $OUTPUT_FILE &&
        echo -e "Completed run with $cores cores in $t seconds"

    fi

done

# Move output file to another directory
mv $OUTPUT_FILE outputs/$OUTPUT_FILE
