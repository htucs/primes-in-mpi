# Import libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# Read data file
data = pd.read_csv("output-fixed.dat", skiprows=1, header=None)

# Extract columns
cores = data[0]
time = data[1]

# Capture initial time for 1 core
time_1_core = time[0]

# Calculate the times taken for a "perfect" parallelism
time_perfect = time_1_core / np.array([1, 2, 4, 8, 16, 32, 64, 128])

# Create y-scale
y_pos = np.arange(len(cores.values))

# Create a figure
fig = plt.figure(figsize=(8,6))

# Create a "perfect" line with points
plt.plot(y_pos, time_perfect, color="black", label="Optimal Parallelization")

# Create a barplot
plt.bar(y_pos, time, label="Actual Results")

# Annotations, ticks and labels
plt.xticks(y_pos, (1, 2, 4, 8, 16, 32, 64, 128))
plt.xlabel("No. of CPUs")
plt.ylabel("Time Elapsed (s)")
plt.suptitle("Computation Time vs No. of CPUs")
plt.title("128-Core Zaina Cluster")
plt.legend()

# Save image to file
fig.savefig("plots/time_vs_cpu_barline_fixed.png")